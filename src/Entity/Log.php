<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LogRepository")
 */
class Log
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $instruction_letter;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $driver;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Truck")
     * @ORM\JoinColumn(nullable=false)
     */
    private $truck;

    /**
     * @ORM\Column(type="integer")
     */
    private $amount;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $load_location;

    /**
     * @ORM\Column(type="time")
     */
    private $time_depart;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $location;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $event;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\user")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInstructionLetter(): ?string
    {
        return $this->instruction_letter;
    }

    public function setInstructionLetter(?string $instruction_letter): self
    {
        $this->instruction_letter = $instruction_letter;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getDriver(): ?user
    {
        return $this->driver;
    }

    public function setDriver(?user $driver): self
    {
        $this->driver = $driver;

        return $this;
    }

    public function getTruck(): ?truck
    {
        return $this->truck;
    }

    public function setTruck(?truck $truck): self
    {
        $this->truck = $truck;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getLoadLocation(): ?string
    {
        return $this->load_location;
    }

    public function setLoadLocation(string $load_location): self
    {
        $this->load_location = $load_location;

        return $this;
    }

    public function getTimeDepart(): ?\DateTimeInterface
    {
        return $this->time_depart;
    }

    public function setTimeDepart(\DateTimeInterface $time_depart): self
    {
        $this->time_depart = $time_depart;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getEvent(): ?string
    {
        return $this->event;
    }

    public function setEvent(string $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function getUser(): ?user
    {
        return $this->user;
    }

    public function setUser(?user $user): self
    {
        $this->user = $user;

        return $this;
    }
}
