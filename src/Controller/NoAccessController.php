<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class NoAccessController extends AbstractController
{
    /**
     * @Route("/pagenotfound", name="noaccess")
     */
    public function index(Request $request)
    {

        return $this->render('no_access/index.html.twig', [
            'controller_name' => 'NoAccessController',
        ]);
    }
}
